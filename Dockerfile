FROM python:3-slim

RUN apt update && apt install -y build-essential libpq-dev

COPY ./*.whl ./
RUN pip install *.whl && rm -rf *.whl

#EXPOSE 5000
CMD ["python", "-m", "xyz-app.app"]