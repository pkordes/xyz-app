CI_REGISTRY_IMAGE 	?= registry.gitlab.com/pkordes/xyz-app
IMAGE_NAME 			?= xyz-app
IMAGE_TAG 			?= $(shell git rev-parse --short HEAD)
IMAGE_FULL_TAG 		:= ${CI_REGISTRY_IMAGE}:${IMAGE_TAG}
3MIMAGE_NAME		:= xyz-3m
3MIMAGE_FULL_TAG	:= ${CI_REGISTRY_IMAGE}/${3MIMAGE_NAME}:latest
AWS_PROFILE 		?= default
DOCKERRUN 			:= docker run -it -v ${PWD}:/app -e AWS_PROFILE -w /app --rm ${CI_REGISTRY_IMAGE}/${3MIMAGE_NAME}:latest
GETVERSION 			:= docker run -v ${PWD}:/app -e AWS_PROFILE -w /app --rm ${CI_REGISTRY_IMAGE}/${3MIMAGE_NAME}:latest poetry version | cut -d " " -f 2
APP_HOST			?= 127.0.0.1
APP_PORT			?= 5001
VERSION				?= 0.1.4
INFRARUN	 		:= docker run -v $(HOME)/.helm/:/root/.helm/ -v $(HOME)/.aws/:/root/.aws/:rw -v $(HOME)/.kube:/root/.kube:rw -v $(HOME)/.docker:/root/.docker:ro \
						-v $(PWD):/app/ -w /app --rm ${CI_REGISTRY_IMAGE}/${3MIMAGE_NAME}:latest

.DEFAULT_GOAL := help

.PHONY: help
help: ## Show this help
	@echo "Usage: make [target]"
	@echo "Available targets:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: build3m-image
build3m-image: ## Build the base 3m image
	docker build -f Dockerfile.3m -t ${3MIMAGE_FULL_TAG} .

.PHONY: push3m-image
push3m-image: ## Build the base 3m image
	docker push ${3MIMAGE_FULL_TAG}

.PHONY: 3mshell
3mshell: ## Shell into 3M image
	$(DOCKERRUN) /bin/bash

.PHONY: build
build: ## Poetry build
	$(DOCKERRUN) make _build
.PHONY: _build
_build: #_update
	poetry build

.PHONY: unittest
unittest: ## Run unit tests
	$(DOCKERRUN) make _unittest

.PHONY: _unittest
_unittest:
	poetry run pytest src/xyz-app/app.py

.PHONY: update
update: ## Poetry update
	$(DOCKERRUN) make _update
.PHONY: _update
_update:
	poetry update

.PHONY: build-image
build-image: build ## Build app container image
	@echo "build container"
	docker build -t ${IMAGE_FULL_TAG} -f ./Dockerfile ./dist

.PHONE: push-image
push-image:
	docker push ${IMAGE_FULL_TAG}

.PHONY: tag-image
tag-image:
	docker tag ${IMAGE_FULL_TAG} ${REGISTRY}/${REGISTRY_OWNER}/${IMAGE_NAME}:$(shell ${GETVERSION})

.PHONE: push-tag-image
push-tag-image:
	docker push ${REGISTRY}/${REGISTRY_OWNER}/${IMAGE_NAME}:$(shell ${GETVERSION})

.PHONY: run
run: ## run the container image
	docker run -p 5001:5000 --rm --name testxyz -d ${IMAGE_FULL_TAG}

.PHONY: stop
stop: ## stop the container image
	docker stop testxyz

.PHONY: test-image
test-image: ## test the built image
	${MAKE} run && sleep 1
	${MAKE} test-curl
	${MAKE} stop

.PHONY: test-curl
test-curl: ##
	@RESPONSE="$(shell curl -s 'http://${APP_HOST}:${APP_PORT}')" && if [ "$$RESPONSE" = "<p>Hello Liatrio</p>" ]; then echo "PASSED";else echo "FAILED" && exit 1; fi

.PHONY: run-local
run-local: ## run the flask app locally without docker
	poetry run flask --app src/xyz-app/app run

.PHONY: tf-init
tf-init: ## terraform init on infra
	$(INFRARUN) make _tf-init
.PHONY: _tf-init
_tf-init:
	cd infra && terraform init

.PHONY: tf-plan
tf-plan: ## terraform plan on infra
	$(INFRARUN) make _tf-plan
.PHONY: _tf-plan
_tf-plan:
	cd infra && terraform plan -out tf.out

.PHONY: tf-apply
tf-apply: ## terraform plan on infra
	$(INFRARUN) make _tf-apply
.PHONY: _tf-apply
_tf-apply:
	cd infra && terraform apply -auto-approve tf.out

.PHONY: tf-destroy
tf-destroy: ## terraform destroy on infra
	$(INFRARUN) make _tf-destroy
.PHONY: _tf-destroy
_tf-destroy:
	cd infra && terraform destroy -auto-approve

.PHONY: kubeconfig
kubeconfig: ## Update kube config from EKS
	$(INFRARUN) make _kubeconfig
_kubeconfig:
	aws eks update-kubeconfig --name xyz-eks

.PHONY: deploy
deploy: ## Helm deploy/upgrade app
	$(INFRARUN) make _deploy
_deploy: _kubeconfig
	helm upgrade xyz-app \
	--set image.tag=$(VERSION) \
	--install \
	--wait \
	./helm-chart

.PHONY: testdeploy
testdeploy: ## TEST FINAL DEPLOY
	$(INFRARUN) make _testdeploy
_testdeploy: _kubeconfig
	APP_HOST=$(shell kubectl get svc xyz-app --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}") APP_PORT=80 \
	${MAKE} test-curl
