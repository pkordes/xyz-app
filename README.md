
<a name="readme-top"></a>

# XYZ Proof of Concept  

## About The Project

This repo takes a very simple Hello World python Flask application and demonstrates pipeline deployment of a container to a cloud infrastructure.  Everything required to create the infrastructure in AWS, containers, and deployment via a pipeline is defined.

The goal is to tackle the following common problems:
* Environment consistency
* Speed of starting new development
* Deployment outages

## Infrastructure 

### EKS - Elastic Kubernetes Service

Terraform is used to create a very simple EKS cluster using code copied from https://github.com/hashicorp/learn-terraform-provision-eks-cluster.

Below is a diagram from the associated [tutorial](https://developer.hashicorp.com/terraform/tutorials/kubernetes/eks) showing the architecture of the deployed cluster:

![](https://content.hashicorp.com/api/assets?product=tutorials&version=main&asset=public%2Fimg%2Fterraform%2Feks%2Foverview.png)

The infrastructure is a one time deployment using the following commands:
* `make tf-init`
* `make tf-plan`
* `make tf-apply`

Cleanup is as simple as:
* `make tf-destroy`

## Getting Started 

### 3-Musketeers Pattern

A common issue during app development and subsequent deployment is lack of consistency.  This is sometimes known more commonly as:

![](https://user-images.githubusercontent.com/6512072/36191276-5bc817c0-116d-11e8-89cb-b5c96c054936.jpg)

Per the https://3musketeers.io website:

>The 3 Musketeers is a pattern for developing software in a repeatable and consistent manner. It leverages Make as an orchestration tool to test, build, run, and deploy applications using Docker and Docker Compose. The Make and Docker/Compose commands for each application are maintained as part of the application’s source code and are invoked in the same way whether run locally or on a CI/CD server.

This basically means there are the minimal prerequisites.

### Prerequisites

The only tools required locally are:
* Make
* Docker
* AWS CLI - configured with sufficient access

In order to deploy locally, a local installation of Kubernetes is required (i.e. Docker Desktop, Minikube)

### Makefile

The Makefile is the key every step of the development process. All targets have a simple description that can be shown by using `make help` or just `make`

Most of the targets are run with a special 3m container that containers make, docker, docker-compose, and all other tools (python, poetry, aws, terraform, helm, etc.).  These targets will call their _equivalent.  For instance `make build` will invoke `_make build` from within the 3m container.

The 3m container is built and pushed from the Dockerfile.3m using:
* `make build3m-image`
* `make push3m-image`

## Local Development

- Build python app via `make build`
- Run python unit tests via `make unittest`
- Build app container image via `make build-image`
- Test app container image via `make test-image`

## Pipeline

[Github Flow](https://docs.github.com/en/get-started/quickstart/github-flow) is a simple branching strategy for getting started with CI/CD principles for new teams.  The Gitlab Pipeline for this project follows this branching strategy for deployment to production according the following flow:

![Github Flow](githubflow.png)

### Feature branch

A feature or bugfix branch is created from master with changes and pushed remotely.  For every commit to this branch the following stages and jobs are created:

![Branch Pipeline](branchpipeline.png)
* build-job: calls `make _build`
* unit-test-job: calls `make _unittest`

The main goal here is for fast feedback.

### Merge request

Once a feature is deemed complete, a Merge Request is created and more thorough testing is done by additionally building the image and testing a running instance:

![MR Pipeline](mrpipeline.png)

New jobs:
* build-push-image-job: builds and pushes images using built app
* test-image-job: starts the container, tests for curl output, shuts down container

### Merge to master

![Master Pipeline](masterpipeline.png)

New jobs:
* prep_release: finds the latest tag and bumps the patch number
* release: pushes the new tag to Gitlab and creates a release

### Tag

Once a new tag is pushed to master it triggers the final pipeline that promotes/retags the latest image and deploys it followed by one final test:

![Tag Pipeline](tagpipeline.png)

New jobs:
* promote-image-job: pulls, retags, pushes latest image
* deploy: calls `make _deploy`
* smoketest: calls `make _testdeploy`

# Additional info:
* EKS via TF: https://developer.hashicorp.com/terraform/tutorials/kubernetes/eks
* 3Musketeers (3M): https://3musketeers.io
* 3M Target vs _Target: https://3musketeers.io/guide/make.html#target-vs-target
* Helm Quickstart: https://helm.sh/docs/intro/quickstart/
* Github flow: http://scottchacon.com/2011/08/31/github-flow.html

<p align="right">(<a href="#readme-top">back to top</a>)</p>
