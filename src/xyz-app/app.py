from flask import Flask, jsonify
import time

app = Flask(__name__)

def getName():
    name = "Liatrio"
    return name

def test_getName():
    assert(getName()=="Liatrio")

@app.route("/")
def hello():
    return '<p>Hello {}</p>'.format(getName())

if __name__ == '__main__':
    app.run(host="0.0.0.0", port="5000")
